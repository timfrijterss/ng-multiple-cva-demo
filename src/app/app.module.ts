import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PercentCVADirective } from './cva/percent-cva/percent-cva.component';
import { RoundCVADirective } from './cva/round-cva/round-cva.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { RoundPercentageCvaComponent } from './cva/percentage-round-cva.component.ts/round-percentage-cva.component';
import { PercentageRoundCvaComponent } from './cva/round-percentage-cva/percentage-round-cva.component';

@NgModule({
  declarations: [
    AppComponent,
    PercentCVADirective,
    RoundCVADirective,
    RoundPercentageCvaComponent,
    PercentageRoundCvaComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
