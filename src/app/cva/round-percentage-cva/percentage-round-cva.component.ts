import { Component, ElementRef, Renderer2, Self, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';

@Component({
  selector: 'app-percentage-round-cva',
  templateUrl: './percentage-round-cva.component.html',
})
export class PercentageRoundCvaComponent implements ControlValueAccessor {

  @ViewChild('input', { static: true }) input!: ElementRef;
  onTouched!: () => void;
  onChange!: (value: string | null) => void;

  percentageFormControl!: FormControl;

  /**
   *  Injecting NgControl so formControlName, formControl directive and NgModel
   *  all work easily. They all provide themselves as NgControl
   */
  constructor(@Self() public controlDir: NgControl, private readonly renderer: Renderer2) {
    controlDir.valueAccessor = this;
    this.percentageFormControl = new FormControl();
  }

  writeValue(value: any): void {
    console.log('writeValue is called [percentage-round-cva]');
    this.percentageFormControl.setValue(value);
    this.renderer.setProperty(this.input.nativeElement, 'value', value !== null ? parseFloat(this.percentageFormControl.value).toFixed(2) : '');
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    console.log('registerOnChange called [percentage-round-cva]');
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
    console.log('registerOnTouched called [percentage-round-cva]');
  }

}
