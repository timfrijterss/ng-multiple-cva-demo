import { Component, ElementRef, Renderer2, Self, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';

@Component({
  selector: 'app-round-percentage-cva',
  templateUrl: './round-percentage-cva.component.html',
})
export class RoundPercentageCvaComponent implements ControlValueAccessor {

  @ViewChild('input', { static: true }) input!: ElementRef;
  onTouched!: () => void;
  onChange!: (value: string | null) => void;

  roundFormControl!: FormControl;

  /**
   *  Injecting NgControl so formControlName, formControl directive and NgModel
   *  all work easily. They all provide themselves as NgControl
   */
  constructor(@Self() public controlDir: NgControl, private readonly renderer: Renderer2) {
    controlDir.valueAccessor = this;
    this.roundFormControl = new FormControl();
  }

  writeValue(value: any): void {
    console.log('writeValue is called [round-percentage-cva]');
    this.roundFormControl.setValue(value);
    this.renderer.setProperty(this.input.nativeElement, 'value', value !== null ? parseFloat(this.roundFormControl.value) / 100 : '');
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    console.log('registerOnChange called [round-percentage-cva]');
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
    console.log('registerOnTouched called [round-percentage-cva]');
  }
}

// ngx-mask + ngbDatePicker cva


// wrapper - cva
// --> ngx-mask (input) + ngbDatePicker (input)
