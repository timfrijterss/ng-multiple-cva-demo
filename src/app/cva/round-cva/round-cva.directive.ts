import { Directive, ElementRef, forwardRef, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';


@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[round-cva]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RoundCVADirective),
      multi: true,
    },
  ],
})
export class RoundCVADirective implements ControlValueAccessor {
  onTouched!: () => void;
  onChange!: (value: string | null) => void;

  /**
   *  Injecting NgControl so formControlName, formControl directive and NgModel
   *  all work easily. They all provide themselves as NgControl
   */
  constructor(private readonly renderer: Renderer2, private elementRef: ElementRef) {
  }

  writeValue(value: any): void {
    console.log('writeValue is called [round-cva]');
    this.renderer.setProperty(this.elementRef.nativeElement, 'value', value !== null ? parseFloat(value).toFixed(2) : '');
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    console.log('registerOnChange called [round-cva]');
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
    console.log('registerOnTouched called [round-cva]');
  }

}
